package com.investree.internship.controller;

import com.investree.internship.model.Peminjam;
import com.investree.internship.model.Transaksi;
import com.investree.internship.repository.MeminjamRepository;
import com.investree.internship.repository.PeminjamRepository;
import com.investree.internship.view.TransaksiServiceImpl;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
@NoArgsConstructor
@RequestMapping("/transaksi/")
public class TransaksiController {
    @Autowired
    private PeminjamRepository peminjamRepo;
    @Autowired
    private MeminjamRepository meminjamRepo;

    private TransaksiServiceImpl transaksiService;
    @PostMapping(value = "/update/{id}")
    public String updateData(Model model,
                             @ModelAttribute(value = "asset")Transaksi transaksi){

        model.addAttribute("asset", transaksi);
        model.addAttribute("peminjam", peminjamRepo.findAll());
        model.addAttribute("meminjam", meminjamRepo.findAll());
        transaksiService.save(transaksi);
        return "redirect:/transaksi/index";
    }
    @PostMapping(value = "/submit")
    public String saved (@ModelAttribute Transaksi transaksi,
                         BindingResult result){
        if (result.hasErrors()){
            return "pages/asset/form";
        }
        transaksiService.save(transaksi);
        return "pages/transaksi/index";
    }
}
