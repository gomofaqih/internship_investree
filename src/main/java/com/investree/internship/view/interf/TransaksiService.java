package com.investree.internship.view.interf;

import com.investree.internship.model.Transaksi;

import java.util.List;
import java.util.Optional;

public interface TransaksiService {

    public List<Transaksi> getAll ();
    public Transaksi save (Transaksi returs);
    public Optional<Transaksi> findById (Long id);
    public void delete(Long id);
}
