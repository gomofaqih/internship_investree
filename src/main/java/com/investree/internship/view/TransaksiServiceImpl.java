package com.investree.internship.view;

import com.investree.internship.model.Transaksi;
import com.investree.internship.repository.TransaksiRepository;
import com.investree.internship.view.interf.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class TransaksiServiceImpl implements TransaksiService {
    @Autowired
    private TransaksiRepository transaksiRepo;

    public List<Transaksi> getAll (){
        return transaksiRepo.findAll();
    }
    public Transaksi save (Transaksi returs){
        return transaksiRepo.save(returs);
    }
    public Optional<Transaksi> findById (Long id){
        return transaksiRepo.findById(id);
    }
    public void delete(Long id){
        transaksiRepo.deleteById(id);
    }
}
