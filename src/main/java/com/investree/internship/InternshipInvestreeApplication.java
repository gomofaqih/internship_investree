package com.investree.internship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InternshipInvestreeApplication {

	public static void main(String[] args) {
		SpringApplication.run(InternshipInvestreeApplication.class, args);
	}

}
