package com.investree.internship.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaksi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer tenor;

    @Column(name = "total_pinjaman")
    private Double totalPinjaman;

    @Column(name = "bunga_persen")
    private Double bungaPersen;

    private String status;

    @ManyToOne
    @JoinColumn(name = "id_Peminjam")
    private Peminjam peminjam;

    @ManyToOne
    @JoinColumn(name = "id_meminjam")
    private Meminjam meminjam;

}
