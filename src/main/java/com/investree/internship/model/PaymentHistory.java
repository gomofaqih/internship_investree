package com.investree.internship.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "payment_history")
@Builder
public class PaymentHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pembayaran_ke")
    private Integer pembayaranKe;

    private Double jumlah;

    private String bukti_pembayaran;

    @ManyToOne
    @JoinColumn(name = "id_transaksi")
    private Transaksi transaksi;

}
