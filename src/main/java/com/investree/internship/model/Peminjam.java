package com.investree.internship.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_peminjam")
@Builder
public class Peminjam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_peminjam")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_userDetail")
    private UserDetail userDetail;
}
