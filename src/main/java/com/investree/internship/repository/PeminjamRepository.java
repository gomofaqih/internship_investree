package com.investree.internship.repository;

import com.investree.internship.model.Peminjam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeminjamRepository extends JpaRepository<Peminjam, Long> {
}
