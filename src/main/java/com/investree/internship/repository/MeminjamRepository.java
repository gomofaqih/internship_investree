package com.investree.internship.repository;

import com.investree.internship.model.Meminjam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeminjamRepository extends JpaRepository<Meminjam,Long> {
}
